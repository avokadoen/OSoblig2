# OSoblig2

## description
Aksel Hjerpbakk 997816

Nikolai Åkerholt 473184

We used shellcheck to confirm the quality of our script

Shellcheck has a guide for installing at: https://github.com/koalaman/shellcheck#installing

To use spellcheck say "spellcheck *scriptname*.bash" in terminal

Secondly we used bash -n scriptname.bash to check for syntax


## How to run on ubuntu
Simply navigate clone repo and run "./*scriptname*.bash" in your terminal.
if it fails to run try "chmod +x *scriptname*.bash" 
